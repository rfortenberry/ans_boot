#Update packages and upgrade
pacman -Syu --noconfirm

#Install ansible and git
pacman -S --noconfirm ansible git

#Clone workstation repo
ansible-pull -U https://gitlab.com/rfortenberry/ans_work